<?php
function registration_complete() {
    global $error_validation, $username, $password, $first_name, $last_name;
    if ( 1 > count( $error_validation->get_error_messages() ) ) {
        $userdata = array(
         'first_name' =>   $first_name,
         'last_name' =>   $last_name,
         'user_login' =>   $username,
         'user_pass' =>   $password,
 
        );
        $user = wp_insert_user( $userdata );
        add_user_meta($user, 'place_of_birth', $_POST['place_of_birth']);
        add_user_meta($user, 'date_of_birth', $_POST['dob']);
        add_user_meta($user, 'gender', $_POST['gender']);
        update_user_meta($user, 'description', $_POST['bio']);

        echo 'Registration successful. Go to <a href="' . get_site_url() . '/wp-login.php">Login</a>.';
    }
}
?>