# Wordpress Custom Registration Plugin

## Description
    A plugin that allows users to register with additional fields such as gender, date of birth e.t.c

## Author
    Samuel Aronda

## Installation & Usage

    1. Add the plugin to your wordpress folder 
    2. Login into the Admin Dashboard. Go to the plugins section and activate the plugin
    3. Create a Registration Page on the pages section 
    4. Add shortcode [sc_registration_form] to page created

## Video Links
   1.[Registration & Login Process](https://drive.google.com/file/d/1QA0DeTcOI7iJ4r5biCPbB9qGXcwFp6gE/view?usp=sharing)
   2.[Edit User Page](https://drive.google.com/file/d/1lhPLwWUi1KaibyfZ_ZJFABtbYs2988gl/view?usp=sharing)


## Security considerations 
    1. Password encryption- password is encrypted and not stored as plaintext in the database
    2. Keep error messages generic
    3. A user doesn't already exist with a certain username
    4. Validating and sanitizing user inputs 

## Plugins vs Themes
    Themes alter the site’s layout and visual style of the website while plugins are additional features or functionality that you can add to improve the efficiency, security and performance of your WordPress website

