<?php
/*
  Plugin Name: Custom Registration Form
  Description: Custom registration form that will allow users to register with additional fields.
  Version: 1.0
  Author: Samuel Aronda 
 
 */
require('form_validation.php');
require('registration_complete.php');
require('custom_registration_form.php');
include('display_fields.php');

function registration_form_function() {
    global $first_name, $last_name,$username, $password, $email, $place_of_birth;
    if ( isset($_POST['submit'] ) ) {
        form_validation(
         $_POST['username'],
         $_POST['password'],
         $_POST['email'],
         $_POST['fname'],
         $_POST['lname'],
         $_POST['place_of_birth'],
         $_POST['bio'],
         $_POST['gender'],
         $_POST['dob']
       );
 
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $first_name =   sanitize_text_field( $_POST['fname'] );
        $last_name  =   sanitize_text_field( $_POST['lname'] );
        $place_of_birth = sanitize_text_field( $_POST['place_of_birth'] );



       registration_complete(
         $username,
         $password,
         $first_name,
         $last_name
        );
    }
    custom_registration_form(
        $username,
        $password,
        $first_name,
        $last_name,
        $place_of_birth,
        $gender,
        $dob,
        $bio
    );
}

add_shortcode( 'sc_registration_form', 'shortcode_registration' );
function shortcode_registration() {
    ob_start();
    registration_form_function();
    return ob_get_clean();
}
