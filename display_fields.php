<?php

// Wordpress Hook to edit_user_profile with custom fields
add_action('edit_user_profile', 'display_custom_fields');

function display_custom_fields( $user ) {
?>
    <table class="form-table">
    <tbody>
        <tr>
            <th>
                <label for="Place of Birth"><?php _e( 'Place of Birth' ); ?></label>
            </th>
            <th>
                <label for="Gender"><?php _e( 'Date of Birth' ); ?></label>
            </th>
            <th>
                <label for="Date of Birth"><?php _e( 'Gender' ); ?></label>
            </th>
            </tr>
            <tr>
            <td>
                <input type="text" name="place_of_birth"  value="<?php echo esc_attr( get_the_author_meta( 'place_of_birth', $user->ID ) ); ?>" class="regular-text" />
            </td>

            
            <td>
                <input type="date" name="dob"  value="<?php echo esc_attr( get_the_author_meta( 'date_of_birth', $user->ID ) ); ?>" class="regular-text" />
            </td>

            
            <td>
                <input  type="text" name="gender"  value="<?php echo esc_attr( get_the_author_meta( 'gender', $user->ID ) ); ?>" class="regular-text" disabled />
            </td>
        </tr>
        </tbody>
    </table>
<?php
}

?>