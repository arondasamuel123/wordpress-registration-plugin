<?php

function form_validation( $username, $password)  {
    global $error_validation;
    $error_validation = new WP_Error;

    if ( empty( $username ) || empty( $password )){
        $error_validation->add('field', 'Required form field is missing');
    }
    if ( username_exists( $username ) ) {
    $error_validation->add('user_name', 'Sorry, that user already exists!');
    }

    if ( 6 > strlen( $password ) ) {
        $error_validation->add( 'password', 'Password length must be greater than 6' );
    }

    if ( is_wp_error( $error_validation ) ) {
 
        foreach ( $error_validation->get_error_messages() as $error ) {
         
            echo '<div>';
            echo '<strong>ERROR</strong>:';
            echo $error . '<br/>';
            echo '</div>';
             
        }
     
    }
}
?>